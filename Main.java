import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import coinpurse.Coin;
import coinpurse.ConsoleDialog;
import coinpurse.Purse;

 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Natanon Poonawagul
 */
public class Main {
	private static int CAPACITY = 10;
    /**
     * @param args not used
     */
    public static void main( String[] args ) {
//    	Purse purse = new Purse(CAPACITY);
//    	ConsoleDialog coinConsole = new ConsoleDialog(purse);
//    	coinConsole.run();
    	List<Coin> coins = new ArrayList<Coin>();
//    	Coin c9 = new Coin(9);
//    	coins.add(c9);
//    	coins.add(new Coin(1));
//    	coins.add(new Coin(11));
//    	coins.add(new Coin(99));
//    	System.out.println(coins.contains(c9));
//    	System.out.println(coins.contains(new Coin(99)));
//    
    	for(int i = 0;i<10;i++)
    		coins.add(new Coin((int) Math.round(Math.random()*100)));
    	
    // Sort a obj inside the list
    Collections.sort(coins);

    	for(int i =0;i<10;i++) 
    System.out.println(coins.get(i).getValue());

    }
}
