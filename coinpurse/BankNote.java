package coinpurse;
/**
 * A BankNote with value.
 * @author Natanon Poonawagul
 *
 */
public class BankNote extends AbstractValuable{
	private long currentSerial;
	public static long nextSerialNumber = 1000000;
	/**
	 * Constructor for this BankNote.
	 * @param value of this BankNote.
	 * @param currency of this BankNote.
	 */
	public BankNote(double value,String currency){
		super(value,currency);
		this.currentSerial = nextSerialNumber;
		nextSerialNumber++;
	}
	/**
	 * A method return serial number for next BankNote.
	 * @return serial number for next BankNote.
	 */
	public long getNextSerialNumber(){
		return nextSerialNumber;
	}
	/**
	 * A Method describe this BankNote.
	 * @return description for this BankNote.
	 */
	public String toString(){
		return this.value + " " + this.currency + " BankNote [" + this.currentSerial + "]";
	}
}

