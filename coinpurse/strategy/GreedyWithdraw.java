package coinpurse.strategy;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import coinpurse.Valuable;
import coinpurse.ValueComparator;
/**
 * WithdrawStrategy by using greedy algorithm.
 * @author Natanon Poonawagul
 *
 */
public class GreedyWithdraw implements WithdrawStrategy<Valuable> {
	ValueComparator VALUECOMPARE = new ValueComparator();
	/**
	 * An implements method using for withdraw money in purse by using greedy algorithm.
	 * @param amount is the amount of money you want to withdraw.
	 * @param valuables is the list of value in the purse.
	 * @return Array of values from BankNotes Coins or Coupons that use for the amount of withdraw.
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> temp = new ArrayList<Valuable>();
		Collections.sort(valuables,VALUECOMPARE);
		for(int i = valuables.size()-1;i >= 0;i--){
			if(amount - valuables.get(i).getValue() >= 0)	{
				amount -= valuables.get(i).getValue();
				temp.add(valuables.get(i));
			}
		}
		if ( amount > 0 )
		{
			return null;
		}
		return temp;
	}
	
}
