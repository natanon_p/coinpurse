package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * A withdrawStrategy using Recursion to solve the problems.
 * @author Natanon Poonawagul
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy<Valuable>{

	/**
	 * A withdraw method using Recursion.
	 * @param amount is the amount to withdraw.
	 * @param list of items that purse contains.
	 * @return the solution as items to withdraw, or null if can't withdraw the amount.
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> list){
		if(list.size()==0) return null;
		List<Valuable> temp2 = withdrawFrom(amount,list,list.size()-1);
		if(temp2==null)
			return withdraw(amount,list.subList(0, list.size()-1));
		return temp2;
	}
	/**
	 * A helper method using recursion running like probability. 
	 * @param amount is amount to withdraw.
	 * @param list of items that purse contains. 
	 * @param lastIndex is the index of last item in the list. 
	 * @return null if it can't withdraw otherwise return a list with items.
	 */
	public List<Valuable> withdrawFrom(double amount, List<Valuable> list,int lastIndex){
		if(amount < 0){
			return null;
		}
		if(lastIndex < 0 && amount != 0){
			return null;
		}
		double thisValue = list.get(lastIndex).getValue(); 
		if(amount - thisValue == 0){
			List<Valuable> finalTemp = new ArrayList<Valuable>();
			finalTemp.add(list.get(lastIndex));
			return finalTemp;
		}
		List<Valuable> temp = withdrawFrom(amount-thisValue,list,lastIndex-1);
		if(temp == null) return withdrawFrom(amount,list,lastIndex-1);
		temp.add(list.get(lastIndex));
		return temp;
	}

}
