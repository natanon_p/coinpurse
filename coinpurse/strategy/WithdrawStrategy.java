package coinpurse.strategy;

import java.util.List;

/**
 * An interface use to modify withdraw method.
 * @author Natanon Poonawagul
 *
 * @param <Valuable>
 */
public interface WithdrawStrategy<Valuable> {
	/** withdraw method use to withdraw money from the purse. 
	 * @param amount is the amount of money you want to withdraw from purse.
	 * @param valuables is the list of Value from BankNotes Coins and Coupon.
	 * @return list of BankNotes Coins or Coupons that use for this withdraw amount.
	 */
	List<Valuable> withdraw(double amount,List<Valuable> valuables);
}
