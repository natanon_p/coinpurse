package coinpurse;

import java.awt.Font;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Observer that show purse balance.
 * @author Natanon Poonawagul
 *
 */
public class PurseBalanceObserver implements Observer{
	JLabel label;
	/**
	 * A new constructor for this Observer.
	 */
	public PurseBalanceObserver(){
		init();
	}
	/**
	 * A method use for update this GUI.
	 * @param subject is the observer for this GUI.
	 * @param info is the purse that updated.
	 */
	public void update(java.util.Observable subject, Object info){
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			double balance = purse.getBalance();
			label.setText("Balance is: "+ balance);
		}
		if(info != null) System.out.println(info);
	}
	/**
	 * A method initialize GUI for this observer.
	 */
	public void init(){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		label = new JLabel("Balance is: 0");
		label.setFont(new Font("Herculanum", Font.PLAIN, 18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		frame.add(label);
		frame.setSize(200,60);
		frame.setVisible(true);
	}
}
