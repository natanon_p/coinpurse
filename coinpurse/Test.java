package coinpurse;

public class Test {
	public static void main (String [] args){
		MoneyFactory mf = MoneyFactory.getInstance();
		Valuable coin1 = mf.createMoney(5);
		Valuable coin2 = mf.createMoney("1");
		System.out.println(coin1);
		System.out.println(coin2);
	}
}
