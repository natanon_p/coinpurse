package coinpurse;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
/**
 * Observer that show purse status.
 * @author Natanon Poonawagul
 *
 */
public class PurseStatusObserver implements Observer{
	JLabel label;
	JProgressBar bar;
	Purse purse;
	/**
	 * New constructor for Observer.
	 * @param purse that use this observer.
	 */
	public PurseStatusObserver(Purse purse){
		this.purse = purse;
		init();
	}
	/**
	 * A method initialize GUI for this observer.
	 */
	public void init(){
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		label = new JLabel("Empty");
		label.setFont(new Font("Hoefler Text",Font.ITALIC,18));
		bar = new JProgressBar(0,purse.getCapacity());
		bar.setStringPainted(true);
		bar.setValue(0);
		panel.add(label);
		panel.add(bar);
		frame.add(panel);
		frame.setSize(200,60);
		frame.setVisible(true);
	}
	/**
	 * A method use for update this GUI.
	 * @param subject is the observer for this GUI.
	 * @param info is the purse that updated.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			purse = (Purse) info;
			if(purse.isFull())
				label.setText("FULL");
			else if(purse.count() != 0)
				label.setText("Slot(s) left = "+ (purse.getCapacity() - purse.count()));
		}
		bar.setValue(purse.count());
	}
}
