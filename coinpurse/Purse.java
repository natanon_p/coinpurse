package coinpurse;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A purse contains items.
 *  You can insert items, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the purse decides which
 *  items to remove.
 *  
 *  @author Natanon Poonawagul
 */
public class Purse extends Observable{
	/** Collection of items in the purse. */

	ArrayList<Valuable> list = new ArrayList<Valuable>();
	/** Capacity is maximum NUMBER of items the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;
	/** Strategy that using for withdraw method. */
	private WithdrawStrategy strategy;
	/** 
	 *  Create a purse with a specified capacity and adding observer.
	 *  @param capacity is maximum number of items you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		addObserver(new PurseBalanceObserver());
		addObserver(new PurseStatusObserver(this));
	}

	/**
	 * Count and return the number of items in the purse.
	 * This is the number of items, not their value.
	 * @return the number of items in the purse
	 */
	public int count() { 
		return list.size(); 
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for(int i = 0;i < list.size();i++)
			balance += list.get(i).getValue();
		return balance; 
	}

	/**
	 * Return the capacity of the purse.
	 * @return the capacity
	 */

	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		if(this.count() == this.getCapacity()){
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		return false;
	}

	/** 
	 * Insert a item into the purse.
	 * The item is only inserted if the purse has space for it
	 * and the item has positive value.  No worthless coins!
	 * @param value is a object to insert into purse
	 * @return true if item inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {
		if(this.isFull())
			return false;
		if(value.getValue() <= 0)
			return false;
		list.add(value);
		super.setChanged();
		super.notifyObservers(this);
		return true;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Items withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Item objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		Collections.sort(list);
		if(amount <= 0)
			return null;
		strategy = new GreedyWithdraw();
		ArrayList<Valuable> temp = (ArrayList<Valuable>) strategy.withdraw(amount, list);
		if(temp == null){
			strategy = new RecursiveWithdraw();
			temp = (ArrayList<Valuable>) strategy.withdraw(amount, list);
		}
		if(temp == null)
			return null;
		for(int i = 0;i < temp.size();i++){
			for(int j = 0;j < list.size();j++){
				if(temp.get(i).getValue() == list.get(j).getValue() && temp.get(i).getClass() == list.get(j).getClass()){
					list.remove(j);
					super.setChanged();
					super.notifyObservers(this);
					break;
				}
			}
		}
		Valuable[] itemArr = new Valuable [temp.size()];
		temp.toArray(itemArr);
		return itemArr;
	}
	/**
	 * A method using to set withdrawStrategy to use in withdraw method.
	 * @param strategy to set in WithdrawStrategy.
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy;
	}
	/** 
	 * toString returns a string description of the purse contents.
	 * @return string description of the purse.
	 */
	public String toString() {
		return String.format("%d items with value %.2f",list.size(),this.getBalance());
	}

}

