package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Natanon Poonawagul
 */
public class Coin extends AbstractValuable{


	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 * @param currency is the currency for the coin.
	 */
	public Coin( double value ,String currency) {
		super(value,currency);
	}
	/**
	 * a method that format a String for this coin.
	 * @return String that describe about this coin.
	 */
	public String toString(){
		return this.value + " " + this.currency + " Coin";
	}
}

