package coinpurse;
/**
 * A Factory class for Thai money.
 * @author Natanon Poonawagul
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{
	/** An array checking coinValue.*/
	private double[] coinValue = {1,2,5,10};
	/** An array checking bankValue.*/
	private double[] bankValue = {20,50,100,500,1000};
	/** A currency for Thai money.*/
	private final String Baht = "Baht";
	@Override
	public Valuable createMoney(double value) {
		for(double val: coinValue)
			if(value == val)
				return new Coin(value,Baht);
		for(double val: bankValue)
			if(value == val)
				return new BankNote(value,Baht);
		throw new IllegalArgumentException();
	}
	
}
