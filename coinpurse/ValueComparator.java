package coinpurse;

import java.util.Comparator;
/**
 * Comparator that used to compare value of Items in purse.
 * @author Natanon Poonawagul
 *
 */
public class ValueComparator implements Comparator<Valuable> {
	/**
	 * A method compare two values.
	 * @param a is the first value to be compared.
	 * @param b is the second value to be compared.
	 * @return negative number if value of a less than value of b return zero if their value are the same otherwise return positive number.
	 */
	public int compare(Valuable a,Valuable b){
		if(a.getValue() == b.getValue())
			return 0;
		if(a.getValue() < b.getValue())
			return -1;
		else 
			return 1;
	}
}
