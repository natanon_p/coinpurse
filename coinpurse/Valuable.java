package coinpurse;
/**
 * An interface for getting value.
 * @author Natanon Poonawagul
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * Method that getting value from object.
	 * @return value of variable.
	 */
	public double getValue();

}
