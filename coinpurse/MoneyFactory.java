package coinpurse;

import java.util.ResourceBundle;
/**
 * A Factory class for money current.
 * @author Natanon Poonawagul
 *
 */
public abstract class MoneyFactory {
	/** Instance MoneyFactory. */
	private static MoneyFactory instance;
	/**
	 * A method get MoneyFactory from properties file.
	 * @return instance MoneyFactory.
	 */
	public static MoneyFactory getInstance(){
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String factoryclass = bundle.getString("MoneyFactory");
		if(instance == null){
			try {
				instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return instance;
	}
	/** A abstract method createMoney from value.
	 * @param value is the value for new money object.
	 * @return money object that have the value from parameter.
	 */
	public abstract Valuable createMoney(double value);
	/**
	 * A method createMoney from String parse to value.
	 * @param value is the input String for parsing to value.
	 * @return money object that have the value from parameter.
	 */
	public Valuable createMoney(String value){
		double val = 0;
		try{
			val = Double.parseDouble(value);
		} catch (NumberFormatException e){
			System.out.println(e.getMessage());
		}
		return getInstance().createMoney(val);
	}
}
