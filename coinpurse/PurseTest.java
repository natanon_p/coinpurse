package coinpurse;

import java.util.Arrays;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
/**
 * An application class for testing the purse object.
 * @author Natanon Poonawagul
 *
 */
public class PurseTest {
	/** main method use to test the purse. */
	public static void main (String [] args){
		/** Create purse */
		Purse purse = new Purse(100);
		System.out.println(purse);
		/** Create ConsoleDialog */
		ConsoleDialog console = new ConsoleDialog(purse);
		console.run();
	}
}
