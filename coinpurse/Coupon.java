package coinpurse;
/**
 * A Coupon with value depends on color.
 * You can't change the value and also color of the coupon.
 * @author Natanon Poonawagul
 *
 */
public class Coupon extends AbstractValuable{
	/** Color of coupon. */
	private String color;
	/** Type of coupon seperate by colors and each color has different value. */
	enum CouponType{ RED(100),BLUE(50),GREEN(20);
		public final double value;
		CouponType(double value){
			this.value = value;
		}
	}; 
	/**
	 * Constructor for a new Coupon.
	 * @param color of this coupon (in deep value of this coupon too).
	 * @param currency for coupon.
	 */
	public Coupon(String color,String currency){
		super(CouponType.valueOf(color.toUpperCase()).value,currency);
		this.color = color.toUpperCase();
	}
	/**
	 * A Method describe the coupon.
	 * @return String that describe this coupon.
	 */
	public String toString(){
		return this.color + " Coupon";
	}
}
