package coinpurse;
/**
 * An AbstractValuable which is a superclass of BankNote ,Coin and Coupon objects.
 * @author Natanon Poonawagul
 *
 */
public abstract class AbstractValuable implements Valuable{
	/** value of AbstractValuable. */
	protected double value;
	/** currency of AbstractValuable.*/
	protected String currency;
	/**
	 * Constructor for AbstractValuable.
	 * @param value is the value of AbstractValuable.
	 * @param currency is the currency of AbstractValuable.
	 */
	public AbstractValuable(double value,String currency){
		this.value = value;
		this.currency = currency;
	}
	/**
	 * A method check whether this Valuable has the same value as the other Valuable or not.
	 * @param obj is the other Valuable compare to this Valuable.
	 * @return true if their value are the same false otherwise.
	 */
//	public boolean equals(Object obj){
//		if(obj == null)
//			return false;
//		if(this.getClass() != obj.getClass())
//			return false;
//		Valuable other = (Valuable)obj;
//		if(this.getValue() == other.getValue())
//			return true;
//		return false;
//	}
	/**
	 * an implements method which compare two Valuable.
	 * @param other is the object to compare with.
	 * @return negative number if this Object have less value than other coin positive number if it have more value than other coin otherwise return zero
	 */
	public int compareTo(Valuable other){
		Valuable obj = other;
		if(this.getValue() < obj.getValue())
			return -1;
		else if(this.getValue() == obj.getValue())
			return 0;
		return 1;
	}
	/**
	 * A method return value for this Valuable.
	 * @return value for this Valuable.
	 */
	public double getValue(){
		return this.value;
	}

}
