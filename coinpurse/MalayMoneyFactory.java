package coinpurse;
/**
 * A factory class for malaysian money.
 * @author Natanon Poonawagul
 *
 */
public class MalayMoneyFactory extends MoneyFactory{
	/** Array for checking coinValue. */
	private final double[] coinValue = {0.05,0.10,0.20,0.50};
	/** Array for checking bankValue. */
	private final double[] bankValue = {1,2,5,10,20,50,100};
	/** Currency for coin.*/
	private final String Sen = "Sen";
	/** Currency for BankNote.*/
	private final String Ringgit = "Ringgit";
	@Override
	public Valuable createMoney(double value) {
		for(double val: coinValue){
			if(value == val){
				return new Coin(value*100,Sen);
			}
		}
		for(double val: bankValue){
			if(value == val){
				return new BankNote(value,Ringgit);
			}
		}
		throw new IllegalArgumentException();
	}
}
